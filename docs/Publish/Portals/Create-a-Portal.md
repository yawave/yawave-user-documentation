# Create a Portal

Learn how to create a Portal below.

1. Click *Publish/Portals*.

![Portals](../../../assets/images/Publish/Portals/clickportals.png)

2. Click *Create Portal*.

![Portal](../../../assets/images/Publish/Portals/clickcreateportal.png)

3. Enter the portal name in the *Portal* field (**Note:** This field has an 80 character limit.) If an additional language is needed, click *Add Translation* and select a language from the drop-down menu.

![Portal Name](../../../assets/images/Publish/Portals/enterportalname.png)

4. Enter a description for the portal in the *Description* field (**Note:** This field has a 160 character limit.).

INSERT IMAGE

5. Upload a thumbnail by clicking the *Thumbnail* image box. **Note:** You can also drag and drop an image over the *Thumbnail* image box.

INSERT IMAGE

6. Upload a cover image by clicking the *Cover* image box. **Note:** You can also drag and drop an image over the *Cover* image box.

INSERT IMAGE

7. Enter a URL-friendly version of the portal name in the *Permalink Settings* field. **Note:** The *Permalink Settings* field is automatically populated with the *Portal* name you entered in Step 1 above. This field is also referred to as a slug.

INSERT IMAGE

Yawave also shows you the *URL Preview* using the slug you entered.

INSERT IMAGE
