# Create a Publication

Learn how to create a Publication below.

1. Click *Publish/Publications*.

![Publications](../../../assets/images/Publish/Publications/clickpublications.png)

2. Click *Create Publication*.

![Publication](../../../assets/images/Publish/Publications/clickcreatepublication.png)
