# Branding

Click **Administrate > Brand** to configure a brand, logo, and theme for your application.

![Branding Selection](../../../assets/images/Branding/Branding.png)

- See [here](Configuring-a-Brand.md) for information to configure a brand and logo for your application.

- See [here](Configuring-a-theme.md) for information to configure a theme for your application.

