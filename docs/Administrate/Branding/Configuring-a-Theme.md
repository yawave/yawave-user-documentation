# Configuring a Theme

Click **Administrate > Branding > Theme** to configure a theme for your application.

![Theme](../../../assets/images/Branding/Theme.png)


1. Click **General** to configure the following.


   ![General](../../../assets/images/Branding/Theme_General.png)


   a. In **Theme name**, enter a name for your theme.


   b. In **Primary color**, select a color to be used as a primary color for any actions or links appearing in the                application.


   c. In **Secondary color**, select a color to be used as a secondary color for text items appearing in the                  application.


   d. In **Background**, select a color to be used as a background color for the application.


2. Click **Header** to configure the following.

   ![Header](../../../assets/images/Branding/Theme_Header.png)

   a. In **Background**, select a color to be used as a background color for the header area in the application.


   b. In **Icons, typography**, select a color to be used for icons in the application.


3. Click **Portal Cover** to configure the following.


   ![Portal Cover](../../../assets/images/Branding/Theme_PortalCover.png)

   a. Use the slider to show or hide the portal cover, portal name, portal description, and portal image. 


   b. In **Custom cover height**, enter the height in pixels for the portal cover.



4. Click **Publication Detail** to configure the following.


   ![Publication Detail](../../../assets/images/Branding/Theme_PublicationDetail.png)


   a. In **Background**, select a color to be used as a background color for any publication in the application.


   b. In **Hide Header**, use the slider to show or hide the header for the publication.


   c. In **Related Content**, select the publication types for which the Related Content block will be visible. 


   d. In **Number of Publications**, select **All** to show the list of all publications or select **Custom** to show         specified number of publications. 

      If you have selected *Custom*, use the slider to provide the number of publications to be listed.

  
5. Click **Publication Cover** to configure the following.


   ![Publication Cover](../../../assets/images/Branding/Theme_PublicationCover.png)


  a. In **Cover layout style**, select a layout style for the publication cover.


  b. In **Cover corner radius (px)**, enter the number of pixels for the publication cover corners.


  c. Use the slider to show or hide image, title, description, category and subcategory, date and author, views and          activities count, and share panel for the publication cover.



6. Click **Fonts** to configure the following.


   ![Fonts](../../../assets/images/Branding/Theme_Fonts.png)


   a. In **Paragraph**, select the font and font style to be used for text in the paragraphs.


   b. In **Heading**, select the font and font style to be used for text in the headings.


7. Click **Form** to configure the following.


   ![Form](../../../assets/images/Branding/Theme_Form.png)

  
   a. In **Background**, select a color to be used as a background color for the form elements in the application.


   b. In **Icons, typography**, select a color to be used for icons in the form.


   c. In **Corner radius (px)**, enter the number of pixels for the form corners.



8. Click **Save** to save the changes done; otherwise click **Cancel**.
     




