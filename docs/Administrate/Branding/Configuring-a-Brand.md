# Configuring a Brand

Click **Administrate > Branding > Brand** to configure a brand and logo for your application.

![Configure Brand](../../../assets/images/Branding/ConfigureBrand.png)


1. Configure the following in **Basic Information**.

   ![Basic Information](../../../assets/images/Branding/Branding_BasicInfo.png)

    a. In **Application Title**, enter a title for your application. An example of the application title appears to the     right.
    

    b. In **Tagline**, enter a description about your application. An example of the tagline is appears to the right.
    

    c. In** Logo Image - Dark**, and **Logo Image - Light**, drag and drop the required image for your application logo,       or click to select an image from your local computer. An example of the application logo in the dark mode and           light mode appears to the right. 
    

    d. In **Favicon**, drag and drop the required image to appear as a favicon, or click to select an image from your          local computer. An example of the favicon appears to the right. 


     *Notes*:

      * Click ![Upload](../../../assets/images/Branding/ImageUploadIcon.png) to upload another image as the application         logo or favicon.*

      * Click ![Delete](../../../assets/images/Branding/ImageDeleteIcon.png) to delete the uploaded image.*


    e. In **Application Slug**, enter a text that will be used as a URL for the application. 


2. Configure the following in **Subdomain Information**.
  
   ![Subdomain Information](../../../assets/images/Branding/Branding_Subdomain.png)


   Enter the text to be used as a subdomain on Yawave.


3. Configure the following in **Application Navigation**.

   ![Application Navigation](../../../assets/images/Branding/Branding_ApplicationNavigation.png)


   In **Home**, enter the URL to be linked to from your home page.


4. Configure the following in **Contact Information**.

   ![Contact Information](../../../assets/images/Branding/Branding_ContactInfo.png)


   Enter your email address, phone number, and fax number in **Email**, **Phone**, and **Fax** respectively.


5. Configure the following in **Location**.

   ![Location](../../../assets/images/Branding/Branding_Location.png)


   Use the slider to turn on the location, and provide your address. 


6. Click **Save** to save the configuration done; otherwise click **Cancel**.