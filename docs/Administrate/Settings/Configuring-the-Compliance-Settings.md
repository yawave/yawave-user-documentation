# Configuring-the-Compliance-Settings

Click **Administrate > Settings > Compliance** to configure the compliance and privacy settings. 

![Compliance](../../../assets/images/Settings/Settings_Compliance.png)


1. Click **Add Language** to select the required language to configure the compliance settings.
   
   Click X corresponding to the language to delete the language. A confirmation message appears, where you must click      **Delete** to continue; otherwise click **Cancel**.


2. In **Permission Type**, select the type of opt-in requirement.
    
   - Select *Single Opt-In* to if you do not wish to send a confirmation email to thae subscribed contacts.

   - Select *Confirmed Opt-In* to send a confirmation email to contacts after they have subscribed.

   - Select *Double Opt-In* to send a confirmation email to contacts twice to confirm the subscription.


3.  In **Purpose**, enter a description that will appear to contacts in their subscription confirmation email. This         appears only if the **Permission Type** is selected as *Double-Opt-In*.


4. Provide the following information in **Permission Parameters**.


   - Enter a label and the URL to access the terms of using the application in **Your terms of use label** and **Your        terms of use link** respectively.

   - Enter a label and the URL to access the cookie policy in **Your cookie policy label **and **Your cookie policy          link** respectively. 

   - Enter a label and the URL to access the privacy policy in **Your privacy policy label** and **Your privacy 
     policy link** respectively.


5. Provide the following information in **Permission Text**.


   - In **Permission text without checkmark**, enter the permission text to be accepted by the subscribers to confirm        the data privacy and terms of use. This appears without a checkbox.


   - Use the slider in **Permisson checkmarks** to show or hide a check box for the permission text.


   - In **Permission text with checkmark**, enter the permission text to be accepted by the subscribers to confirm           the data privacy and terms of use. This appears with a checkbox. 


   - In **Permission error message**, enter the error message to be displayed if any. 


6. In **Inherit compliance settings**, use the slider to turn on and apply these compliance settings to all                applications in the organization. Otherwise, you can configure compliance settings for each application in the          organization.


7. Click **Save** to save the compliance settings; otherwise click **Cancel**.

