# Settings

Click **Administrate > Settings** to configure details such as compliance and privacy settings, email service provider, social media settings, and notification settings.


![Settings](../../../assets/images/Settings/Settings.png)


