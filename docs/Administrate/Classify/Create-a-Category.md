# Create a Category

Learn how to create a Category below.

1. Click *Administrate/Classify/Categories*.

![Categories](../../../assets/images/Classify/clickcategories.png)

2. Click *Create Category*.

![Category](../../../assets/images/Classify/clickcreatecategory.png)

3. Enter the category name in the *Name* field. **Note:** Click *Add Translation* and select a language from the drop-down menu.

![Category Name](../../../assets/images/Classify/entercategoryname.png)

4. By default *Use as Interest* is enabled. This feature allows your contacts to subscribe to interests in their notification settings. Click the toggle once to disable this feature.

![Interest](../../../assets/images/Classify/defaultuseinterest.png)

5. Select a default icon from the drop-down menu (1) or upload your own icon by clicking *Upload* (2).

![Add Icon](../../../assets/images/Classify/selectoraddicon.png)

6. Enter a URL-friendly version of the category name in the *Slug* field. **Note:** The *Slug* field is automatically populated with the *Category* name you entered in Step 1 above.

![Enter Slug](../../../assets/images/Classify/enterslug.png)

7. Click *Create*.

![Click Create](../../../assets/images/Classify/clickcreate.png)


