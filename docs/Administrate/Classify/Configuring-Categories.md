# Configuring Categories


Click **Classify > Categories** to configure categories to associate publications in your application.


![Categories](../../../assets/images/Classify/Classify_CategoryList.png)



## Creating a New Category

Click **Create Category** in the Categories page to create a new category.

![New Category](../../../assets/images/Classify/Classify_NewCategory.png)


1. In **Name**, enter a name for the category. 


2. Click **Add Translation** to select the required language to translate the categories.
   
   Click ![Delete](../../../assets/images/GettingStarted/DeleteIcon.png)corresponding to the language to delete the        language.


3. In **Use as Interest**, use the slider to turn on of turn off this setting. If enabled, this category will be tagged    as an interest and all publications in this category will be shared to the contacts.


4. In **Icon**, select an icon for this category, or click Upload to upload an image from your local computer for this     category.


5. In **Slug**, enter a text that will be used as a URL for the category.


6. In **Attribute**, select the attributes to be associated to this category.


7. Click **Create** to create this category; otherwise click **Cancel**.



## Viewing and Managing Categories

You can execute the following in the Categories page.


![Categories](../../../assets/images/Classify/Classify_CategoryList.png)


1. Details such as category name, number of subcategories in the category, usage of the category, the category slug,       organization, interest of contacts, inheritance, and state of the category appears. 

   Use ![Navigate](../../../assets/images/Classify/NavigateIcon.png) to navigate to navigate to the category details.

   *Note - You can use the slider to turn on or turn off the interest and state of the category in Interest and State       respectively.*


2. Click **Filter** to enter a text based on which you require to filter the categories.


3. Click ![Select](../../../assets/images/Classify/SelectIcon.png) corresponding to a category to select the following.


   a. Select **Edit** to update the category; click **Save** to save the changes done.


   b. Select **Delete** to delete the category. A confirmation message appears, where you must click **Delete** to            delete the category; otherwise click **Cancel**.


4. In **Subcategories**, click **Manage** to view and manage subcategories within this category. 

  *Note - Click **Create Subcategory** to create a new subcategory within this category. See **Creating a New              Category** in this section for more information.*






  




