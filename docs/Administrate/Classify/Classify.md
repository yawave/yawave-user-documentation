# Classify

Click **Administrate > Classify** to configure categories, attributes and tags to be associated to publications in your application.


![Classify](../../../assets/images/Classify/Classify.png)