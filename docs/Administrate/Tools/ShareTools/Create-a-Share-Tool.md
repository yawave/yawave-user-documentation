# Create a Share Tool

Learn how to create a Share Tool below or [edit the default Share Tool](../../../../docs/Administrate/Tools/ShareTools/Edit-the-Default-Share-Tool.md) created when you signed up for a Yawave account.

1. Click *Administrate/Tools/Share Tools*.

![Share Tools](../../../../assets/images/Administrate/Tools/clicksharetools.png)

2. Click *Create Tool*.

![Create Tool](../../../../assets/images/Administrate/Tools/clickcreatetool.png)

3. Click *Share*.

![Share](../../../../assets/images/Administrate/Tools/clickshare.png)

4. Enter a name of your choosing. **Note:** This field has a 66 character limit.

![Share Tool Name](../../../../assets/images/Administrate/Tools/entersharetoolname.png)

5. Select a *Journey* from the drop-down menu or select *Add Journey* to create a new one (NEED MORE INFORMATION).

6. Select *Add Touchpoint* from the drop-down menu (NEED MORE INFORMATION).

7. To segment contacts acquired with your new share tool, assign a contact to a dimension by selecting (WHERE DO YOU CREATE DIMENSIONS?) 