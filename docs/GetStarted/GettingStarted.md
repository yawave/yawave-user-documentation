# Getting Started

Yawave is a Content Marketing Application that allows you to generate content, and publish across channels such as websites, social media, newsletter, or Liveblog. This application also helps to evaluate the content and track individual contribution towards the content. You can also define user roles and permissions, and design the branding and theming for your content.


## Launching the Application as a First Time User

If you are a first time user, execute the following steps to launch and sign up to the application.

1. Go to https://yawave.com/, and click **Log In**.


![Login](../../assets/images/GettingStarted/LogIn.png)


2. Click **Sign Up** to create a new account.


![Sign Up](../../assets/images/GettingStarted/FirstTimeUserSignUp1.png)


3. Enter details such as the first name, last name, email address, and password to login to the application; reenter the password for confirmation, and click **Sign Up** to create the account.


![Sign Up Details](../../assets/images/GettingStarted/FirstTimeUserSignUp2.png)


4. The user activation email is sent to the specified email address. Follow instructions in the email to activate your account.


5. Login to the application after you have activated the account; see **Signing into the Application** below for more information. 


6. Configure your application by providing the following details, and then click **Continue**.


   - In **Application name**, enter a name for the application. 


     - Click **Add Translation** to select and add a transation language to the application.


     - Click ![DeleteIcon](../../assets/images/GettingStarted/DeleteIcon.png) to delete the application.



   - In **Application slug**, enter a text that will be used as a URL for the application.


7. Select the required subscription plan for your application, and click **Upgrade**. Enter the required details to save your subscription.


![Application Plan](../../assets/images/GettingStarted/SelectPlan.png)


## Signing into the Application

Execute the following steps to sign into the application:

1. Go to https://yawave.com/, and click **Log In**.


![Login](../../assets/images/GettingStarted/LogIn.png)



2. Enter the login credentials (that is, the email address and password), and then click **Log In** to login to the application.


![Login To Application](../../assets/images/GettingStarted/LoginToApp.png)


3. Select the **Remember me** check box if you require the computer to remember your credentials for logging into the application in the future.


## Logging out of the Application

In the application Dashboard, click the user name appearing to the right of the application, and then select **Log Out** to sign out of the application.

![Log Out](../../assets/images/GettingStarted/LogOut.png)

