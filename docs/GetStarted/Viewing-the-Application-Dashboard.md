# Viewing-the-Application-Dashboard

Login to the application to view the application dashboard; see [here](Getting Started.md) for information on logging into Yawave. 


![Dashboard](../../assets/images/GettingStarted/Dashboard.png)

The Dashboard shows details such as the URL to access your application, number of publications in your application, 
number of team members, the application subscription plan, and the list of publications created in your application.


The following are available in the Dashboard:


1. **Content App** shows the URL to access your application.

    Click **Create new** to create a new publication, collection, portal, or live blog. 



2. **Subscription plan** shows the subscription details to your application. 

    Click **Upgrade to Starter** to update your subscription plan.



3. **Most Activated Publications** shows the list of publications in your application. Details such as publication name    number of recipients, number of activities executed for the publication, number of visits to the publication, and       additional details on the publication like feedback, and number of media shares appears.


