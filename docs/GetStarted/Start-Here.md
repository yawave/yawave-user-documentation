# Start Here

We've made it easy for you to onboard Yawave quickly. Here you'll learn the steps you need to take to start publishing content on your website.

## Onboarding Steps


**Step 1.** [Create](../../docs/GetStarted/Create-a-Yawave-Account.md) a Yawave account.

**Step 2.**	[Create](../../docs/Administrate/Tools/ToolsOverview.md) and configure a Tool.

**Step 3.** [Create](../../docs/Administrate/Classify/Create-a-Category.md) and configure Categories.

**Step 4.** [Create](../../docs/Publish/Portals/Create-a-Portal.md) and configure Portal.

**Step 5.** [Create](../../docs/Publish/Publications/Create-a-Publication.md) and configure Publication.

**Step 6.** Embed your new Portal on your website (link to xxxxx page). Another option is to embed a Tool on your website (link to xxxxx page).

**Step 7.** [Configure Branding](../../docs/Administrate/Branding/Configuring-a-Brand.md).

**Step 8.** [Configure Compliance Settings](../../docs/Administrate/Branding/Configuring-a-Theme.md).



<!-- theme: info -->

>### Questions or Need Assistance?
>
>[Create a ticket.](https://yawavedev.atlassian.net/servicedesk/customer/portal/3/user/login?destination=portal%2F3) A Yawave support specialists will respond to your ticket as soon as possible.
