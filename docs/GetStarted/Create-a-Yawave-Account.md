# Create a Yawave Account

Follow the steps below to create a Yawave account.

1. Go to https://yawave.com/ and click *TRY FREE*.

![Click TRY FREE](../../assets/images/GettingStarted/clicktryfree.png)

2. You have two options when creating your account:

   **Option 1**: Add your credentials manually (enter First Name, Last Name, Email Address, Password/Confirm Password and then click Sign Up) or

   **Option 2**: Use your social media credentials (click either Facebook, Twitter, LinkedIn, or Google and follow the on screen instructions)

![Sign Up Options](../../assets/images/GettingStarted/signupoptions.png)

3. If you chose **Option 1** in Step 2 above, you'll see a screen similar to the one below. Follow the instructions on the screen to complete your account signup.

![Check your email instructions](../../assets/images/GettingStarted/checkyouremail.png)

4. Enter the name you want to use for your first application. The name you choose can be anything.

![Enter Application Name](../../assets/images/GettingStarted/enterapplicationname.png)

5. Enter the name you want to use for your first application's slug. The name you choose can be anything. If a slug name is already taken by another Yawave user, you'll see a message stating the slug is already taken. Slug names have a 200 character limit.

![Enter Application Slug Name](../../assets/images/GettingStarted/enterapplicationslugname.png)

6. Click *Continue*.

7. The **Plan** page loads. The default plan selected is the **Free** plan. Click *Try Free* to continue with the **Free** plan or click *Upgrade* under any of the paid plans.

![Click Try Free Plan](../../assets/images/GettingStarted/clicktryfreeplan.png)

8. When the Yawave dashboard opens you'll see a welcome message. Click *Schedule Demo* for a 30 minute free introduction or click *Close* to continue to the Yawave dashboard.

![Click Schedule Demo](../../assets/images/GettingStarted/scheduledemo.png)

<!-- theme: info -->

>### Next Steps
>
>[Create and configure a Tool](../../docs/Administrate/Tools/ToolsOverview.md) or review [Onboarding Steps](../../docs/GetStarted/Start-Here.md).


